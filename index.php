<?php
require "vendor/autoload.php";

use src\Routes\Route;
use src\Controllers\RegisterController;
// use src\Controllers\UserController;
// use src\DB\Access;
// use src\DB\Model;
// use src\Models\UserModel;
// use src\DB\MyDatabase;
// use src\Controllers\LoginController;

Route::use('api/register', function($post) {
    RegisterController::create($post);
});

// MyDatabase::connect();
// Model::query();

?>