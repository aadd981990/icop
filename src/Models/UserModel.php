<?php
namespace src\Models;

use src\DB\Model;

class UserModel extends Model{
 
    public string $name;
    public string $lastname;
    public string $email;
    public string $password;
    
    public function __construct(string $name, string $lastname, string $email, string $password)
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->password = $password;  
    }

    public function createUser()
    {
        parent::create("users", $this);
    }
}

