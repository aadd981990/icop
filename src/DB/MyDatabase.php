<?php
namespace src\DB;

use src\DB\Access;
use PDO;
use PDOException;
class MyDatabase {
    public static $connetion;
    public function __construct(){
        self::connect();
    }
    public static function connect(){
        $config = Access::getConnectionInfo();
        $servername = $config['host'];
        $username = $config["user"];
        $password = $config["password"];
        $dbname =$config["database"];
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try {
            self::$connetion = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, $opt);
        } catch (PDOException $e) {
            echo 'PDO Connection failed: ' . $e->getMessage();
            exit();
        }
         
    }
    public static function getConnection(){
        if (self::$connetion instanceof PDO) {
            return self:: $connetion;
        }
        self::connect();
        return self::$connetion;
    }
}

