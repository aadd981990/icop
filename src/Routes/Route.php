<?php

namespace src\Routes;

  class Route {

  private static $routes = Array();
  private static $pathNotFound = null;
  private static $methodNotAllowed = null;

  private static function isValidJSON($str) {
    json_decode($str);
    return json_last_error() == JSON_ERROR_NONE;
  }

  public static function use($url = '/', $function){
    $parsed_url = parse_url($_SERVER['REQUEST_URI']);//Parse Uri

    if(isset($parsed_url['path'])){
      $path = $parsed_url['path'];
    }else{
      $path = '/';
    }
    
    $pathArray = explode('/api', $path);
    if(!empty($pathArray[1])) {
      $currentPath = $pathArray[1];
      $finalUrl = "api{$currentPath}";
      if($url === $finalUrl) {
        if($_SERVER["REQUEST_METHOD"] === "POST") {
          $json_params = file_get_contents("php://input");
          if (strlen($json_params) > 0 && self::isValidJSON($json_params)){
            $decoded_params = json_decode($json_params);
            call_user_func($function, $decoded_params);
          } else {
            //bad request
            http_response_code(400);
            exit("Is not JSON");
          }
        }elseif ($_SERVER["REQUEST_METHOD"] === "GET") {
          call_user_func($function);
        }   
      }
    } 

  }
}

?>