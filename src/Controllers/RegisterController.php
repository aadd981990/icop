<?php
namespace src\Controllers;

use src\Models\UserModel;
class RegisterController
{
    public static function create($data)
    {

        if (!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
            http_response_code(400);
            exit("Invalid email format");
        }

        if (empty($data->name) || empty($data->lastName) || empty($data->password)) {
            http_response_code(400);
            exit("All  fields are required "); 
        }
            
        $userModel = new UserModel($data->name,$data->lastName,$data->email,$data->password);
        $userModel->createUser();
    }
}